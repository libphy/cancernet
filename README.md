# cancernet

## About
Cancer is a very complex disease which involves interactions between multiple genes, aberrant expression of regulatory genes (e.g. lncRNAs), etc. 
Recent popular approaches in predicting gene-disease association utilize heterogeneous data sources and network analysis. In terms of the network data creation, 
it can be grouped into two categories: 1) Using gene expression and gene-gene (or protein-protein) interaction network (PPI), 
and 2) including non-coding RNA data such as lncRNA and miRNA to predict lncRNA and disease associations.
This project use heterogeneous protein-protein, protein-gene and gene-gene interaction data and use graph neural network models to predict cancer from genes.
It includes 6 most common/deadly cancer types in men and women- ovarian cancer, breast cancer, lung cancer, colon cancer, prostate cancer, and pancreatic cancer, 
which data has been collected from multiple sources such as DisGeNet, HumanNet, NPInter, etc. 

## Project status
This project has not been updated since 2022 July.     
As a pilot, it started with binary classification of cancer of each cancer type. It will expand to do multiclass classification in the future. 
Currently, GraphSAGE and GAT models have been implemented but it will expand in the future.

