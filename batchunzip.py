import glob
import os
import subprocess
import argparse


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', type=str, help="data directory")
    parser.add_argument('-e', type=str, help="file extension. e.g. zip, gz, targz")
    args = parser.parse_args()
    datadir = args.d
    extension = args.e
    flist = glob.glob(datadir+'/*.'+extension)
    
    if args.e == 'zip':
        for f in flist:
            if not os.path.isdir(f[:-4]):
                subprocess.run(["unzip", f, '-d', f[:-4]]) #-d option will create a folder with the same name as compressed
    elif args.e == 'gz':
        subprocess.run(["gzip", "-dr", datadir]) #it will not create a folder with the same name as compressed
    elif args.e == 'targz':
        for f in flist:
            subprocess.run(["tar", "-xf",f])    
    else:
        print("compression file extension not available.")
            
    # usage example 1. run batchunzip.py -d data/NCBI_HumanNetPI -e zip
    # usage example 2. run batchunzip.py -d data/NPInter_v4 -e gz