#requires pip install ncbi-datasets-pylib

import pandas as pd
from typing import List
import os
import time
from ncbi.datasets.openapi import ApiClient as DatasetsApiClient
from ncbi.datasets.openapi import ApiException as DatasetsApiException
from ncbi.datasets import GeneApi as DatasetsGeneApi

def getdata(gene_ids: List[int], start: int, end: int):
    if len(gene_ids) == 0:
        print("Please provide at least one gene-id")
        return

    with DatasetsApiClient() as api_client:
        gene_api = DatasetsGeneApi(api_client)

        # download a data package with FASTA files
        try:
            print("Begin download of data package ...")
            gene_ds_download = gene_api.download_gene_package(
                gene_ids, include_annotation_type=["FASTA_GENE"], _preload_content=False
            )
            gene_reply = gene_api.gene_metadata_by_id(gene_ids)
            zipfile_name = "data/NCBI_HumanNetPI/genedata_"+str(start)+"_"+str(end)+".zip"

            with open(zipfile_name, "wb") as f:
                f.write(gene_ds_download.data)
            print(f"Download completed -- see {zipfile_name}")

        except DatasetsApiException as e:
            print(f"Exception when calling GeneApi: {e}\n")

if __name__=='__main__':
    if not os.path.isdir("data/NCBI_HumanNetPI"):
        os.mkdir("data/NCBI_HumanNetPI")
    df_humanPI = pd.read_csv('data/HS-PI.tsv', delimiter='\t', header=None)
    humanPI_genes = sorted(list(set(df_humanPI[0]).union(set(df_humanPI[1]))))
    num_samples = len(humanPI_genes)
    chunk = 500
    print(num_samples,'human genes in HumanNet')
    t0=time.time()
    for i in range(2000,3000,chunk):
        try: 
            getdata(humanPI_genes[i:i+chunk], i, min(i+chunk,num_samples))
        except:
            print(i, "An error occurred. Trying again")
            time.sleep(30)
            try:
                getdata(humanPI_genes[i:i+chunk], i, min(i+chunk,num_samples))
            except:
                print(i, "Second try failed. Could not download. Skipping", i, '-', i+chunk)
        t1=time.time()
        print(i,'-', min(i+chunk,num_samples), t1-t0, 's elapsed.')
    mins, secs = divmod(t1-t0, 60)
    hrs, mins = divmod(mins,60)
    print(hrs,'hrs', mins,'mins',secs,'secs')
    