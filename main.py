##To do: this is copied on notebook. display need to change in scripts

#from utils import *
from datagen import *
from train import *
import matplotlib.pyplot as plt
%matplotlib inline

if __name__=='__main__':
    datadir = 'data/torchdata'
    ## To do: add load function for pre-saved data

    cancertype = 'ovarian' #ovarian, breast, lung, prostate, colon, pancreatic

    for dataargs in [{'cancertype':cancertype, 'datadir':datadir, 'test_size':0.3},]:
        dataargs = objectview(args)   

    print("Create the "+cancertype+" cancer dataset")    
    dataset = create_dataset(dataargs)


    ## Training
    for args in [
        {'model_type': 'GraphSage', 'dataset': 'ovarian', 'num_layers': 2, 'heads': 1, 'batch_size': 32, 'hidden_dim': 32, 'dropout': 0.5, 'epochs': 10, 'opt': 'adam', 'opt_scheduler': 'none', 'opt_restart': 0, 'weight_decay': 5e-3, 'lr': 0.01},
    ]:
        args = objectview(args)
        for model in ['GraphSage']:
            args.model_type = model

            # Match the dimension.
            if model == 'GAT':
                args.heads = 2
            else:
                args.heads = 1

            ## To do: add codes for loading chosen dataset

            test_accs, losses, best_model, best_acc, test_loader = train(dataset, args) 

            print("Maximum test set accuracy: {0}".format(max(test_accs)))
            print("Minimum loss: {0}".format(min(losses)))

            # Run test for our best model to save the predictions!
            test(test_loader, best_model, is_validation=False, save_model_preds=True, model_type=model)
            print()

            plt.title(dataset.name)
            plt.plot(losses, label="training loss" + " - " + args.model_type)
            plt.plot(test_accs, label="test accuracy" + " - " + args.model_type)
        plt.legend()
        plt.show()