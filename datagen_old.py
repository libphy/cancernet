import torch
from torch_geometric.data import Data, InMemoryDataset
from torch_geometric.utils import degree
from sklearn.model_selection import train_test_split
import pandas as pd
import copy
import os
from collections import Counter
import numpy as np

class CancerDataset(InMemoryDataset):
    def __init__(self, data_obj, test_size=None, validation=False, transform=None):
        super(CancerDataset, self).__init__(data_obj, transform, None, None)
        self.data_obj = copy.deepcopy(data_obj)
        
        self.data_obj.num_nodes = len(self.data_obj.x)
        #normalize x
        original_x = copy.deepcopy(data_obj.x)
#         self.data_obj['scaler'] = original_x.max()-original_x.min()
#         self.data_obj['shift'] = original_x.mean()
#         self.data_obj.x = (original_x-self.data_obj['shift'])/self.data_obj['scaler']
        if test_size is not None:
            assert (test_size>0)&(test_size<1), 'Invalid test size. It needs to be 0 < test_size < 1.'
            # splitting the data into train, validation and test
            X_train, X_test = train_test_split(pd.Series(range(self.data_obj.num_nodes)), 
                                                                test_size=test_size, 
                                                                random_state=42)
                
            train_mask = torch.zeros(self.data_obj.num_nodes, dtype=torch.bool)
            test_mask = torch.zeros(self.data_obj.num_nodes, dtype=torch.bool)
            val_mask = torch.zeros(self.data_obj.num_nodes, dtype=torch.bool)
            train_mask[X_train.index] = True
            if validation:
                X_test1, X_test2 = train_test_split(X_test, test_size=0.5) #half of test becomes validation
                test_mask[X_test1.index] = True
                val_mask[X_test2.index] = True
                self.data_obj['val_mask'] = val_mask
            else:    
                test_mask[X_test.index] = True
            
            self.data_obj['train_mask'] = train_mask
            self.data_obj['test_mask'] = test_mask
        else:
            train_mask = torch.ones(self.data_obj.num_nodes, dtype=torch.bool)
            self.data_obj['train_mask'] = train_mask

        self.data, self.slices = self.collate([self.data_obj])

    def _download(self):
        return

    def _process(self):
        return

    def __repr__(self):
        return '{}()'.format(self.__class__.__name__)    

    

class objectview(object):
    def __init__(self, d):
        self.__dict__ = d    
    
    
### standalone functions

def create_data(args, verbose=True):  
 
    if not os.path.isdir(args.datadir):
        os.mkdir(args.datadir)

    #import HumanNet PPI
    HN = pd.read_csv('data/HumanNet/HS-PI.tsv',delimiter='\t',header=None)
    nid = set(list(HN[0])+list(HN[1]))

    #import RH-edges
    RH = pd.read_csv('data/NPInter_v4/processed/RHclean.csv')
    H = set(list(RH.tarEz))

    #import cancer labels
    cancerlabels_dir={'ovarian':"data/DisGeNet/ovarian_cancer.csv",
                      'breast':"data/DisGeNet/breast_cancer.csv",
                      'lung':"data/DisGeNet/lung_cancer.csv",
                      'colon':"data/DisGeNet/colon_cancer.csv",
                      'prostate':"data/DisGeNet/prostate_cancer.csv",
                      'pancreatic':"data/DisGeNet/pancreatic_cancer.csv"
                     }
    cancer = pd.read_csv(cancerlabels_dir[args.cancertype])
    cid = set(list(cancer.EntrezID.unique()))
    print('Cancer Type:', args.cancertype,'cancer')
    
    # create PPI subnetwork 
    HNsub = HN[(HN[0].isin(H))&(HN[1].isin(H))]
    print('PPI subnetwork:', len(set(list(HNsub[0])+list(HNsub[1]))), 'nodes,', len(HNsub), 'edges.') 
    HNsub = HNsub.rename(columns={0:'n1_Ez',1:'n2_Ez',2:'weight'})

    nodes = sorted(list(set(list(HNsub['n1_Ez'])+list(HNsub['n2_Ez']))))

    id2idx = dict(zip(nodes, list(range(len(nodes)))))
    idx2id = dict(zip(list(range(len(nodes))),nodes))

    HNsub['n1_idx'] = HNsub['n1_Ez'].apply(lambda x: id2idx[x])
    HNsub['n2_idx'] = HNsub['n2_Ez'].apply(lambda x: id2idx[x])

    lab=[1 if x in cid else 0 for x in nodes]
    dflabel = pd.DataFrame(list(zip(nodes,lab)),columns=['Ez','cancer'])
    
    #PPI = HNsub[['n1_idx','n2_idx','weight']].sort_values(by='n1_idx')
    PPI = pd.concat([HNsub[['n1_idx','n2_idx','weight']],HNsub[['n2_idx','n1_idx','weight']].rename(columns={'n2_idx':'n1_idx','n1_idx':'n2_idx'})]).drop_duplicates().sort_values(by='n1_idx') #need to include both direction
 
    edges = PPI[['n1_idx','n2_idx']].values.T
    edge_weights = PPI['weight'].values
    print('edges',edges.shape, ', edge weights', edge_weights.shape)

    labels = dflabel.cancer.values
    print('labels', labels.shape)
    
  
    ## add feature extraction
    # Need to filter RH to have H that are in PPI subnetwork
    RHsub = RH[RH.tarEz.isin(nodes)]
    #rnode indices are different from the sample indices. it's feature index
    #Rnodes->features
    #Hnodes->samples (nodes)
    rnodes = sorted(list(RHsub.ncEz.unique()))
    print(len(rnodes))
    id2fidx = dict(zip(rnodes, list(range(len(rnodes)))))
    fidx2id = dict(zip(list(range(len(rnodes))),rnodes))

    RHsub['tar_idx'] = RHsub['tarEz'].apply(lambda x: id2idx[x])
    RHsub['nc_idx'] =  RHsub['ncEz'].apply(lambda x: id2fidx[x])    
    featuremat = np.zeros((len(nodes),len(rnodes)))
    for row in RHsub[['tar_idx','nc_idx']].sort_values(by='tar_idx').values:
        featuremat[row[0],row[1]]=1
    print('node feature matrix:', featuremat.shape)
    
    edge_index = torch.tensor(edges, dtype=torch.long) #already contiguous and right shape
    edge_weights = torch.tensor(edge_weights, dtype=torch.long)
    node_labels = torch.tensor(labels, dtype=torch.long)
    node_features = torch.tensor(featuremat, dtype=torch.float)  #here we use coordinate as node feature

    data = Data(x=node_features, edge_index=edge_index, edge_weights = edge_weights, y=node_labels) 

    return data

def create_dataset(args, verbose=False):
    data = create_data(args, verbose)
    try:
        test_size = args.test_size
    except AttributeError:
        test_size = None
        
    dataset = CancerDataset(data,test_size=test_size,validation=args.validation)
    return dataset


#example data creation
# if __name__=='__main__':
#     datadir = 'data/torchdata'
#     cancertype = 'ovarian' #ovarian, breast, lung, prostate, pancreatic

#     for args in [{'cancertype':cancertype, 'datadir':datadir, 'test_size':0.3},]:
#         args = objectview(args)   
        
#     dataset = create_dataset(args)
