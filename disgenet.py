import pandas as pd
import numpy as np
import requests
import json
import os

class DisGeNet():
    def __init__(self):
        self.token =  json.load(open('bearer.json'))['token']
        self.cids = None
        self.data = None
        self.genedata = None
        self.df = None
        self.df_genes = None
        self.error=False
        self.searchterm = None
        
    def get_disease_ids(self, searchterm):
        my_params = {'term' : searchterm, 'type_id' : 0}
        response = requests.get('https://www.disgenet.org/autocomplete/', params=my_params)             
        cids=[]
        for item in response.json()['original']:
            cids.append(item['id'])

        for item in response.json()['synonyms']:
            cids.append(item['id'])
        return cids    


    def gene_collect(self, cidlist):
        my_headers = {'Authorization' : 'Bearer '+self.token}
        alldata={}
        genes=[]
        it=0
        for cid in cidlist:
            try:
                response = requests.get('https://www.disgenet.org/api/gda/disease/'+cid, headers=my_headers)
                data = response.json()
                alldata.update({cid:data})
                genes.append([cid, [(x['geneid'],x['gene_symbol']) for x in data]])
                if it%10==0:
                    print(it)
                it+=1    
            except:
                self.error=True
                print('Stopped at',it,cid)
                self.savetemp({'alldata':alldata,'genes':genes,'it':it,'cid':cid,'cidlist':cidlist},self.searchterm)
        return alldata, genes    

    def get_data(self, searchterm='ovarian cancer', savegenes=True, savejson=True, savedf=True):
        print('searching', searchterm)
        self.searchterm = searchterm
        if os.path.isdir('data/DisGeNet')==False:
                os.mkdir('data/DisGeNet')
        diseaseids = self.get_disease_ids(searchterm)
        self.cids = diseaseids
        print(len(diseaseids),'cid collected')
        alldata, genes = self.gene_collect(diseaseids)
        if self.error:
            return None
        self.data = alldata
        self.genedata = genes
        
        print("data collected")
        if savejson:
            with open('data/DisGeNet/'+searchterm.replace(' ','_')+".json", "w") as f:
                json.dump(alldata, f)
            print('data json saved')
        # process to pandas dataframe        
        df = pd.DataFrame(genes, columns=['cid','genes'])        
        df['num_genes'] = df['genes'].apply(lambda x: len(x))
        df['gids'] =  df['genes'].apply(lambda x: [y[0] for y in x])
        df['gname'] = df['genes'].apply(lambda x: [y[1] for y in x])
        self.df = df
        if savedf:
            df.to_pickle('data/DisGeNet/'+searchterm.replace(' ','_')+".pkl")
            print('processed df saved')
        genetuples = []
        for x in df.genes:
            genetuples = genetuples+x  
        df_genes = pd.DataFrame(genetuples,columns=['EntrezID','GeneName']).drop_duplicates().reset_index(drop=True)
        self.df_genes = df_genes
        if savegenes:
            df_genes.to_csv('data/DisGeNet/'+searchterm.replace(' ','_')+'.csv')
            print('unique genes csv saved')
        return df_genes

    def savetemp(self, datatemp, fname):
        with open(fname+'_temp.pkl','wb') as f:
            pickle.dump(datatemp,f)
            print('temporary data for', fname, 'saved.')
        
if __name__ == '__main__':
    
#     for term in ['lung cancer','breast cancer','prostate cancer','colon cancer','pancrea cancer']: # it will have connection timeout
    term = 'pancreatic cancer'
    DG = DisGeNet()
    df_gene = DG.get_data(term)
    print(term, len(DG.cids), len(df_gene))
    
